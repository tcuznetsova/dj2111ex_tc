package com.company;

import java.sql.*;

public class Main {


    private static Connection connect = null;
    private static Statement statement = null;
    private static ResultSet resultSet = null;

    private static String url = "jdbc:mysql://localhost:3306/DJ2111Ex";
    private static String user = "root", pass = "1qazasdf";

    public static void main(String[] args) {
        try{
            //this will load the mysql driver, each db has its own driver
            Class.forName("com.mysql.cj.jdbc.Driver");  //right click/open module settings/library/+/path to connector
            connect = DriverManager.getConnection(url,user,pass);
            //statements allow to issue sql queries to the database
            statement = connect.createStatement();
            //resultset get the result of the sql query
            resultSet = statement.executeQuery("select * from Student");


            System.out.println("StudentID " + " Student Name "+ " Student Surname "+ " dataNast ");

            while (resultSet.next()) {
                int studentID = resultSet.getInt(1);
                String studentName = resultSet.getString(2);
                String studentSurname = resultSet.getString(3);
                String dataNast = resultSet.getString(4);
                System.out.println(  studentID + "          " +
                        studentName  +  "         "
                        + studentSurname  + "         "
                        + dataNast  );
            }
        }
        catch (SQLException e){
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}

